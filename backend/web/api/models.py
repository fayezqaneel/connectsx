from django.db import models
from django.contrib.auth.models import User, Group
import hashlib
import json
from django.http import request

# from django.db.models.signals import pre_save



class Hospital(models.Model):
    name = models.CharField(max_length=255)
    contract_enddate = models.DateField(null=True)
    is_trial = models.BooleanField(default=False, null=True)
    contract_info = models.CharField(max_length=255)
    address = models.TextField(null=True)
    password = models.CharField(max_length=255)
    title = models.CharField(max_length=255, null=True)
    # def save(self, *args, **kwargs):
    #     if self.password and self.password != '':
    #         self.password = hashlib.md5(self.password.encode()).hexdigest()
    #     super(Hospital, self).save(*args, **kwargs)
    
    # def create(self, *args, **kwargs):
    #     if self.password and self.password != '':
    #         self.password = hashlib.md5(self.password.encode()).hexdigest()
    #     super(Hospital, self).create(*args, **kwargs)

# def save_profile(sender, instance, **kwargs):
#     print(kwargs)

# pre_save.connect(save_profile, sender=Hospital)


class Conversation(models.Model):
    pincode = models.CharField(max_length=25)
    doctor_firstname = models.CharField(max_length=50)
    doctor_lastname = models.CharField(max_length=50)
    relative_firstname = models.CharField(max_length=50)
    relative_lastname = models.CharField(max_length=50)
    created_at = models.DateField(auto_now_add=True)
    closed_date = models.DateField(null=True)
    hospital = models.ForeignKey("api.Hospital", related_name='hospital', on_delete=models.SET_NULL, blank=True, null=True)


class Message(models.Model):
    message_type = models.CharField(max_length=20)
    message = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    conversation = models.ForeignKey("api.Conversation", related_name='messages', on_delete=models.SET_NULL, blank=True, null=True)
