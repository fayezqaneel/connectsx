import hashlib
from tastypie.resources import ModelResource
from api.models import *
from tastypie import fields, utils
from django.contrib.auth.models import User
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from tastypie.authorization import Authorization
from django.http import HttpResponse
from django.conf.urls import url
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from tastypie.authentication import Authentication

class CustomAuthentication(Authentication):
    def is_authenticated(self, request, **kwargs):
        auth = JWTAuthentication()
        user, token = auth.authenticate(request)
        if user:
            return True
        else :
            return False

    # Optional but recommended
    def get_identifier(self, request):
        return request.user.username


class HospitalResource(ModelResource):
    def obj_update(self, bundle, **kwargs):
        password = bundle.data.get('password')
        if password and password != '':
            bundle.data['password'] = hashlib.md5(password.encode()).hexdigest()
        return super(HospitalResource, self).obj_update(bundle, **kwargs) 

    def obj_create(self, bundle, **kwargs):
        password = bundle.data.get('password')
        if password and password != '':
            bundle.data['password'] = hashlib.md5(password.encode()).hexdigest()
        return super(HospitalResource, self).obj_create(bundle, **kwargs)                

    class Meta:
        queryset = Hospital.objects.all()
        allowed_methods = ['get', 'post', 'delete', 'put']
        authentication = CustomAuthentication()
        authorization = Authorization()
        always_return_data = True
        filtering = {
            "name": ALL,
            "title": ALL,
            "id": ALL,
        }

class ConversationResource(ModelResource):
    hospital = fields.ToOneField('api.resources.HospitalResource', 'hospital', null=True, full=True)
    maessages = fields.ToManyField('api.resources.MessageResource', 'messages', null=True, full=True)
    class Meta:
        queryset = Conversation.objects.all()
        allowed_methods = ['get', 'post', 'delete', 'put']
        authentication = CustomAuthentication()
        authorization = Authorization()
        always_return_data = True
        filtering = {
            "pincode": ALL,
            "doctor_firstname": ALL,
            "doctor_lastname": ALL,
            "hospital": ALL_WITH_RELATIONS,
            "closed_date": ALL,
        }

class MessageResource(ModelResource):
    class Meta:
        queryset = Message.objects.all()
        allowed_methods = ['get', 'post', 'delete', 'put']
        authentication = CustomAuthentication()
        authorization = Authorization()
        always_return_data = True