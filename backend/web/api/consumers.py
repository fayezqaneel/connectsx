from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from api.models import Conversation, Message

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        args = self.scope['url_route']['kwargs']
        self.room_name = '%s-%s' % (args['hospital'], args['pincode'])
        self.hospital = args['hospital']
        self.pincode = args['pincode']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        message = json.loads(text_data)
        conversation = Conversation.objects.filter(hospital__name = self.hospital, pincode= self.pincode, closed_date__isnull=True)
        if conversation:
            conversation = conversation[0]
            Message.objects.create(message_type="text", message=text_data, conversation = conversation)
            # Send message to room group
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message
                }
            )

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))