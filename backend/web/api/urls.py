from django.conf.urls import url, include
from tastypie.api import Api
from api.resources import *
from web.views import me

v1_api = Api(api_name='v1')
v1_api.register(HospitalResource())
v1_api.register(ConversationResource())
v1_api.register(MessageResource())


urlpatterns = [
  # ...more URLconf bits here...
  # Then add:
  url(r'^api/', include(v1_api.urls)),
  url(r'^api/v1/me/$', me, name='user_info'),
]