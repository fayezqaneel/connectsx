import os
import random
import string
import json
import base64
import hashlib
from django.utils.timezone import datetime
from django.core.files.base import ContentFile
from datetime import datetime
from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from api.models import Hospital, Conversation, Message
from django.forms.models import model_to_dict
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.forms.models import model_to_dict


def me(request):
    auth = JWTAuthentication()
    user, token = auth.authenticate(request)
    user.password = ''
    user = model_to_dict(user)
    return JsonResponse(user)

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

@csrf_exempt
def upload(request, hospital, pincode):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        myfile = data.get('file', False)
        fs = FileSystemStorage()
        ext = data.get('ext', False)
        now = datetime.now()
        timestamp = str(datetime.timestamp(now)).split('.')[0]
        myfile = base64.b64decode(myfile)
        # uploaded_file_url = fs.save(timestamp +  ext , myfile)
        imgFile = open('uploads/' + timestamp + '.'+  ext, 'wb')
        imgFile.write(myfile)
        data = {
            "url": 'uploads/' + timestamp + '.'+  ext
        }
        return JsonResponse(data)
    return JsonResponse({})

@csrf_exempt
def close(request, hospital, pincode):
    conversation = Conversation.objects.filter(pincode = pincode, hospital__name=hospital, closed_date__isnull=True)
    if conversation:
        conversation = conversation[0]
        conversation.closed_date = datetime.today()
        conversation.save()
        return HttpResponse('Conversation Closed!', status=200)
    else :
        return HttpResponse('conversation not found !', status=500)
@csrf_exempt
def login(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        print(data)
        hospital = data.get('hospital', False)
        firstname = data.get('firstname', False)
        lastname = data.get('lastname', False)
        hospital_pincode = data.get('hospital_pincode', False)
        mode = data.get('mode', False)
        surgery_pincode = data.get('surgery_pincode', False)
        
        if not hospital:
            return HttpResponse('Hospital name is missing', status=401)
        
        if mode == "doctor" and not hospital_pincode:
            return HttpResponse('Hospital pincode is missing', status=401)
        
        if mode != "doctor" and  not surgery_pincode:
            return HttpResponse('Surgery pincode is missing', status=401)
        
        if not firstname:
            return HttpResponse('Firstname is missing', status=401)
        
        if not lastname:
            return HttpResponse('lastname is missing', status=401)
        now = datetime.now()
        if mode == 'doctor':
            password = hashlib.md5(hospital_pincode.encode()).hexdigest()
            hospital = Hospital.objects.filter(name = hospital, password=password,contract_enddate__gt=now)
            if hospital:
                hospital = hospital[0]
                pincode = randomString(5)
                Conversation.objects.create(
                    doctor_firstname = firstname,
                    doctor_lastname = lastname,
                    pincode = pincode,
                    hospital = hospital
                )
                return JsonResponse({"pincode": pincode})
            else:
                return HttpResponse('Unauthorized', status=401)
        else:
            conversation = Conversation.objects.filter(pincode=surgery_pincode, hospital__contract_enddate__gt=now, hospital__name = hospital, closed_date__isnull=True)
            if conversation:
                conversation = conversation[0]
                # if conversation.relative_firstname != '' and conversation.relative_lastname != '':
                #     return HttpResponse('Unauthorized', status=401)
                    # if conversation.relative_firstname != firstname and  conversation.relative_lastname != lastname:
                    #     return HttpResponse('Unauthorized', status=401)

                conversation.relative_firstname = firstname
                conversation.relative_lastname = lastname
                conversation.save()
                return JsonResponse({"pincode": surgery_pincode})
            else:
                return HttpResponse('Conversation is not exists!', status=401)

@csrf_exempt
def conversation(request):
    if request.method == 'GET':
        pincode = request.GET.get('pincode', False)
        hospital = request.GET.get('hospital', False)
        messages = Message.objects.filter(conversation__pincode = pincode, conversation__hospital__name=hospital, conversation__closed_date__isnull=True)
        conversation_obj = Conversation.objects.filter(pincode = pincode, hospital__name=hospital, closed_date__isnull=True)
        if conversation_obj:
            conversation_obj = conversation_obj[0]
            processed = []
            for message in messages:
                processed.append(model_to_dict(message))
            if conversation_obj:
                return JsonResponse({'messages':processed, 'conversation': model_to_dict(conversation_obj)})
            else:
                return HttpResponse('Not Found', status=404)
    else:
        return HttpResponse('Not Found', status=404)
        # lastname = data.get('lastname', False)
        # hospital_pincode = data.get('hospital_pincode', False)
        # mode = data.get('mode', False)
        # surgery_pincode = data.get('surgery_pincode', False)
        # print(data)
        # if mode == 'doctor':
        #     hospital = Hospital.objects.filter(name = hospital, password=hospital_pincode)[0]
        #     if hospital:
        #         pincode = randomString(5)
        #         Conversation.objects.create(
        #             doctor_firstname = firstname,
        #             doctor_lastname = lastname,
        #             pincode = pincode,
        #             hospital = hospital
        #         )
        #         return JsonResponse({"pincode": pincode})
        #     else:
        #         return HttpResponse('Unauthorized', status=401)
        # else:
        #     conversation = Conversation.objects.filter(hospital__name = hospital, pincode=surgery_pincode)
        #     if conversation:
        #         conversation.relative_firstname = firstname
        #         conversation.relative_lastname = lastname
        #         return JsonResponse({"pincode": pincode})
        #     else:
        #         return HttpResponse('Unauthorized', status=401)