import React, {useState} from 'react';

const SayHello = () => {
    const [name, setName] = useState('Fayez');
    const changeName = (e) => {
        setName(e.target.value);
    }
    return (
        <div>
            Hello {name}<br/>
            <input value={name} onChange={changeName} />
        </div>
    );
};

export default SayHello;