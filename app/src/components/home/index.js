import React from 'react';
import AppImage from '../../images/iphone.png';
import GoogleImage from '../../images/google-icon.png';
import AppleImage from '../../images/apple-icon.png';

const Home = (props) => {

  return (
    <div className="page-holder">
      <div id="container">
        <nav className="navbar">
          <a className="active">Home</a>
          <a href="/privacy">Privacy</a>
          <a href="/contact-us">Contact us</a>
        </nav>
        <div id="background" />

        <div className="section">
        <a href="/"><div className="logo-holder"></div></a>
        <h1 id="title" >Why SurgeryLink?</h1>
        <div id="about-holder">
          SurgeryLink provides a very secure and compliant app for the providers to update the families of the patients during procedures. The chat is a two-way communication. The providers (and a member of the patient family) can enter text, emojis, or upload a picture. The session, text, and images are all deleted from both devices once the session is ended by the provider.
          <div className="app-store-btns">
            <a href="https://play.google.com/store/apps/details?id=com.surgerylink.app" target="_blank"><img src={GoogleImage} /></a>
            <a href=""><img src={AppleImage} /></a>
          </div>
        </div>

        <div className="alta">
          <img id="alta" src={AppImage} />
        </div>
      </div>
      </div>
    </div>
  );

};

export default Home;