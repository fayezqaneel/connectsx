import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Row, Col, Divider, Tabs, Button, Form, Select, Input, Checkbox, Typography, Alert, DatePicker, List, Skeleton, Popconfirm } from 'antd';
import {
  fetchHospitalsAction,
  selectHospitalAction,
  saveHospitalAction,
  addHospitalAction,
  deleteHospitalAction,
  selectHospitalByIdAction,
} from '../../actions/hospital-actions';
import Header from '../../components/shared/header';
import './index.scss';

const { TabPane } = Tabs;
const { Option } = Select;
const { Text } = Typography;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const Hospitals = (props) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  useEffect(() => {
    const { match: { params: { hospital_id } } } = props;
    props.fetchHospitalsAction(() => {
      setLoading(false);
    });
  }, []);

  const deleteHospital = (id, clear_selected) => {
    props.deleteHospitalAction(id);
    if (clear_selected) {
      props.selectHospitalAction(false);
      props.form.resetFields();
    }
  };

  const createHospital = () => {
    props.form.resetFields();
    props.selectHospitalAction(false);
  }

  const editHospital = (hospital) => {
    props.form.resetFields();
    props.selectHospitalAction(hospital);
  }

  const handleSaveHospital = (e) => {
    const { HospitalReducer: { selected_hospital } } = props;
    props.form.validateFieldsAndScroll((err, values) => {
      console.log(selected_hospital.id);
      if (selected_hospital.id) {
        props.saveHospitalAction({ ...values, id: selected_hospital.id }, (data, isError) => {
          if (isError) {
            setError(data.response.statusText);
          } else {
            setSuccess('Hospital was successfully updated!');
          }
        });
      } else {
        props.addHospitalAction({
          ...values,
          randId: Date.now(),
        }, (data, isError) => {
          if (isError) {
            setError(data.response.statusText);
          } else {
            setSuccess('Hospital was successfully updated!');
          }
        });
      }
    });
  }

  const renderHospitals = () => {
    const { HospitalReducer: { hospitals, selected_hospital } } = props;

    return (
      <List
        className="demo-loadmore-list"
        // loading={initLoading}
        itemLayout="horizontal"
        // loadMore={loadMore}
        dataSource={hospitals}
        renderItem={hospital => (
          <List.Item
            actions={[
            <Button type="primary" disabled={loading} onClick={() => editHospital(hospital)}  icon="edit" /> ,
            <Popconfirm
              title="Are you sure delete this hospital?"
              onConfirm={
                () => deleteHospital(
                  hospital,
                  hospital.id == selected_hospital.id
                )
              }
              okText="Yes"
              cancelText="No, cancel"
            >
              <Button
                type="danger"
                icon="delete"
                disabled={loading}
              />
            </Popconfirm>
          ]}
          >
            <Skeleton title={false} loading={loading} active>
              <List.Item.Meta
                title={hospital.name}
              />
            </Skeleton>
          </List.Item>
        )}
      />
    );
  }

  const { getFieldDecorator } = props.form;
  const { HospitalReducer: { hospitals, selected_hospital } } = props;
  return (
    <div id="hospitals-holder">
      <Header active="Hospitals" />
      <div id="main-content-area">
        <div className="hospitals-main-content">
          <Row gutter={16} type="flex" justify="center">
            <Col span={6}>
              <div className="hospitals-lists">
                <div className="card-container">
                  <Tabs type="card">
                    <TabPane tab="Hospitals" key="1">
                      <Button size="large" onClick={() => createHospital()} icon="plus" type="primary" block>Create new hospital</Button>
                      <ul className="quick-links-holder">
                        { renderHospitals() }
                      </ul>
                    </TabPane>
                  </Tabs>
                </div>
              </div>
            </Col>
            <Col span={13}>
              <div className="hospitals-create-edit-area">
                <h3>Create / Edit Hospital</h3>
                <Divider />
                {error && <Alert message={error} type="error" closable />}
                {success && <Alert message={success} type="success" closable />}
                <Form {...formItemLayout} className="hospitals-form">
                <Form.Item label="Hospital Name">
                      {getFieldDecorator('title', {
                        rules: [{ required: true, message: 'Please enter hospital name !' }],
                        initialValue: selected_hospital && selected_hospital.title || '',
                      })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Hospital Username">
                      {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please enter hospital username!' }],
                        initialValue: selected_hospital && selected_hospital.name || '',
                      })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Password">
                      {getFieldDecorator('password', {
                        initialValue: '',
                      })(<Input type="password"/>)}
                  </Form.Item>
                  <Form.Item label="Contract end date">
                    {getFieldDecorator('contract_enddate', {
                      rules: [],
                      initialValue: moment(selected_hospital.contract_enddate),
                    })(
                      <DatePicker
                        format="YYYY-MM-DD"
                        placeholder={'Date'}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...tailFormItemLayout}>
                    {getFieldDecorator('is_trial', {
                      rules: [],
                      valuePropName: 'checked',
                      initialValue: selected_hospital.is_trial
                    })(
                      <Checkbox>Is trail?</Checkbox>
                    )}
                  </Form.Item>
                  <Form.Item label="Contract info">
                      {getFieldDecorator('contract_info', {
                        initialValue: selected_hospital && selected_hospital.contract_info || '',
                      })(<Input.TextArea rows={4} />)}
                  </Form.Item>
                  <Form.Item label="Address">
                      {getFieldDecorator('address', {
                        initialValue: selected_hospital && selected_hospital.address || '',
                      })(<Input.TextArea rows={4} />)}
                  </Form.Item>
                  <Row gutter={16} type="flex" justify="end">
                    <Col md={4}>
                      <Button htmlType="submit" onClick={handleSaveHospital} block type="primary">{ selected_hospital && selected_hospital.id || selected_hospital.randId ? 'Save' : 'Add' }</Button>
                    </Col>
                    { (selected_hospital && selected_hospital.id || selected_hospital.randId)
                      && (
                        <Col md={4}>
                          <Popconfirm
                            title="Are you sure delete this hospital?"
                            onConfirm={
                              () => deleteHospital(
                                selected_hospital,
                                true
                              )
                            }
                            okText="Yes"
                            cancelText="No, cancel"
                          >
                            <Button
                              block
                              type="danger"
                              ghost
                            >
                              Delete
                            </Button>
                          </Popconfirm>
                        </Col>
                      )
                    }
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
  
}

const WrappedHospitalForm = Form.create({ name: 'hospital' })(Hospitals);
const mapStateToProps = ({ HospitalReducer }) => ({ HospitalReducer });
export default connect(mapStateToProps, {
  fetchHospitalsAction,
  selectHospitalAction,
  saveHospitalAction,
  deleteHospitalAction,
  selectHospitalByIdAction,
  addHospitalAction,
})(WrappedHospitalForm);