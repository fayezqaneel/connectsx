import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox, Alert } from 'antd';
import { loginUserAction } from '../../actions/user-actions';
import history from "../../config/history";
import './index.scss';
import logo from './logo.png';

const Login = (props) => {
  const [error, setError] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        props.loginUserAction(values, () => history.push('/hospitals'), error => {
          setError(error.response.data.detail);
        });
      }
    });
  };

  const { getFieldDecorator } = props.form;
  return (
    <div id="login-holder">
      <div className="bk"></div>
      <div className="logo-holder">
        <h1>SurgeryLink</h1>
      </div>
      {error && <Alert message={error} type="error" />}
      <Form onSubmit={handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
              size="large"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
              size="large"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(<Checkbox>Remember me</Checkbox>)}
          <a className="login-form-forgot" href="">
            Forgot password
          </a> */}
          <Button type="primary" size="large" htmlType="submit" className="login-form-button">
            Log in
          </Button>
      
        </Form.Item>
      </Form>
    </div>
  );
  
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
const mapStateToProps = ({ userReducer }) => ({ userReducer });
export default connect(mapStateToProps, { loginUserAction })(WrappedNormalLoginForm);