import React, { useState, useEffect } from 'react';

import {
  Form,
  Input,
  Select,
  DatePicker,
  Checkbox,
  Button,
  Col,
  Row,
  Modal
} from 'antd';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import {
  setMetaAction,
} from '../../actions/conversation-actions';
import {
  fetchHospitalsAction,
} from '../../actions/hospital-actions';
import { addQuery } from '../../config/utils';
import  './search.scss';

const querystring = require('querystring');

const { Option } = Select; 

const formItemLayout = {
  labelCol: { span: 9 },
  wrapperCol: { span: 14 },
};

const formTailLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 16, offset: 7 },
};

const Search = (props) => {
  const [params, setParams] = useState(querystring.parse(props.params.replace(/\?/g, '')));
  const { getFieldDecorator } = props.form;

  useEffect(() => {
    let newParams = params;
    if (params.is_favorite) {
      newParams.status = 'favorites';
    }
    if (params.is_pinned) {
      newParams.status = 'pinned';
    }
    setParams(newParams);
    props.fetchHospitalsAction();
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        addQuery(props, values);
        props.setMetaAction(values);
        // console.log('Received values of form: ', values);
      }
    });
  };

  const renderHospitals = () => {
    const { HospitalReducer: { hospitals } } = props;
    return (
      <Select>
        {hospitals.map(hospital => <Option key={hospital.id} value={hospital.id.toString()}>{hospital.name}</Option>)}
      </Select>
    );
  }

  return (
    <div id="documents-search-holder">
      <Form onSubmit={handleSubmit} layout="horizontal" {...formItemLayout}>
        <Form.Item label="Pincode">
          {getFieldDecorator('pincode__contains', {
            initialValue: params.pincode__contains || ''
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Doctor Firstname">
          {getFieldDecorator('doctor_firstname__contains',
            {
              initialValue: params.doctor_firstname__contains || ''
            }
          )(<Input />)}
        </Form.Item>
        <Form.Item label="Doctor Lastname">
          {getFieldDecorator('doctor_lastname__contains',
            {
              initialValue: params.doctor_lastname__contains || ''
            }
          )(<Input />)}
        </Form.Item>
        <Form.Item label="Hospital">
          {getFieldDecorator('hospital__id',
            {
              initialValue: params.hospital__id ? params.hospital__id.toString() : ''
            }
          )(renderHospitals())}
        </Form.Item>
        <Form.Item {...formTailLayout}>
          <Row gutter={16}>
            <Col md={12}>
              <Button onClick={handleSubmit} block type="primary">
                Search
              </Button>
            </Col>
            <Col md={12}>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </div>
  );
};

const DocumentsSearchForm =  Form.create({ name: 'documents-search' })(Search);

const mapStateToProps = ({ DocumentReducer, HospitalReducer }) => ({ DocumentReducer, HospitalReducer });
export default connect(mapStateToProps, {
  setMetaAction,
  fetchHospitalsAction
 })(withRouter(DocumentsSearchForm));