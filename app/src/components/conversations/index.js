import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Row, Col, Divider, Tabs, Button, Form, Select, Input, Checkbox, Typography, Table, Icon, Modal, DatePicker, List, Skeleton, Popconfirm } from 'antd';
import {
  fetchConversationsAction,
  setMetaAction
} from '../../actions/conversation-actions';
import { withRouter } from "react-router-dom";
import Header from '../../components/shared/header';
import Search from './search';
import './index.scss';
import { addQuery } from '../../config/utils';

const { TabPane } = Tabs;

const Conversations = (props) => {
  const [loading, setLoading] = useState(true);
  const { ConversationReducer: { conversations, selected_conversation } } = props;
  const { getFieldDecorator } = props.form;

  useEffect(() => {
    props.fetchConversationsAction(props.location.search, () => setLoading(false));
  }, [props.location.search]);

  const renderConversationsSearch = () => {
    return (
      <Search params={props.location.search} />
    );
  }

  const changeMeta = (page, limit) => {
    const offset = (page - 1) * limit;
    addQuery(props, { 'offset': offset, limit });
    props.setMetaAction({ offset, limit, page });
  }

  return (
    <div id="conversations-holder">
      <Header active="Conversations" />
      <div id="main-content-area">
        <div className="conversations-main-content">
          <Row gutter={16} type="flex" justify="center">
            <Col span={6}>
              <div className="conversations-lists">
                <div className="card-container">
                  <Tabs type="card">
                    <TabPane tab="Search" key="1">
                      <ul className="quick-links-holder">
                        { renderConversationsSearch() }
                      </ul>
                    </TabPane>
                  </Tabs>
                </div>
              </div>
            </Col>
            <Col span={13}>
              <div className="conversations-create-edit-area">
                <h3>Browse Conversations</h3>
                <Divider />
                <List
                  pagination={{
                    pageSize: props.ConversationReducer.meta.limit,
                    position: 'both',
                    showSizeChanger: true,
                    onChange: changeMeta,
                    current: props.ConversationReducer.meta.page,
                    total: props.ConversationReducer.meta.total_count,
                    showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                    onShowSizeChange: changeMeta
                  }}
                  className="demo-loadmore-list"
                  // loading={initLoading}
                  itemLayout="horizontal"
                  // loadMore={loadMore}
                  dataSource={conversations}
                  renderItem={conversation => (
                    <List.Item
                    >
                      <Skeleton title={false} loading={loading} active>
                        <List.Item.Meta
                          title={<h3>{conversation.hospital.name} - {conversation.pincode}</h3>}
                          description={(
                            <div className="meta-data">
                              <b>Doctor:</b> <span>{conversation.doctor_firstname} {conversation.doctor_lastname}</span>
                              <b>Created:</b> <span>{conversation.created_at}</span>
                              <b>Closed:</b> <span>{conversation.closed_date}</span>
                              <b>Hospital:</b> <span>{conversation.hospital.name}</span>
                            </div>
                          )}
                        />
                      </Skeleton>
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
  
}

const WrappedConversationForm = Form.create({ name: 'conversation_searchl' })(Conversations);
const mapStateToProps = ({ ConversationReducer }) => ({ ConversationReducer });
export default connect(mapStateToProps, {
  fetchConversationsAction,
  setMetaAction
})(withRouter(WrappedConversationForm));