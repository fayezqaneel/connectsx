import React from 'react';

const Support = (props) => {

  return (
    <div className="page-holder">
      <div id="container">
        <nav className="navbar">
          <a href="/">Home</a>
          <a href="/privacy">Privacy</a>
          <a className="active" >Contact us</a>
        </nav>
        <div id="background" />

        <div className="section">
          <a href="/"><div className="logo-holder"></div></a>
          <h1 id="title">Get In Touch</h1>
          <div>
          To get an account or for any support issue, please click on the button below to contact us. We apologize for the inconvenience, but due to the nature of the app usage, tight control of accounts is required.
          </div>
          <a href="mailto:info@surgerylinkapp.com" className="contact-us">Contact us</a>
        </div>
      </div>
    </div>
  );

};

export default Support;