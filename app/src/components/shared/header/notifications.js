import React from 'react';

import { Card } from 'antd';
import { Link } from "react-router-dom";

import './notifications.scss';

const Notifications = (props) => {
  return (
    <div className="notifications-holder">
      <h3>
        Notifications
      </h3>
      <div className="notifications-list">
        <div className="notification-item new">
          <h4 className="title">It is possible that someone may not have been aware of who they were providing assistance to, and may now be hesitant to come forward</h4>
          <div className="meta">8 hours ago</div>
        </div>
        <div className="notification-item">
          <h4 className="title">Four days later, as police searched for clues on the killers and panicked residents in the rural north shuttered their homes</h4>
          <div className="meta">1 min</div>
        </div>
        <div className="notification-item">
          <h4 className="title">A Royal Canadian Air Force Hercules is assisting with the aerial search as police knock on doors in Gillam and Fox Lake Cree Nation over the next few days</h4>
          <div className="meta">Now</div>
        </div>
        <div className="notification-item">
          <h4 className="title">Before authorities described the teens as armed and dangerous suspects</h4>
          <div className="meta">1 year ago</div>
        </div>
      </div>
    </div>
  );
};

export default Notifications;