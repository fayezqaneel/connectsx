import React, { useEffect } from 'react';
import { Icon, Row, Col, Input, Avatar, Badge, Popover, Menu, Button } from 'antd';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { getCurrentUserAction } from '../../../actions/user-actions';

import Notifications from './notifications';
import logo from '../../login/logo.png';
import './index.scss';


const Header = (props) => {
  const { active, userReducer } = props;
  useEffect(() => {
    props.getCurrentUserAction();
  }, []);

  const logout = () => {
    window.localStorage.removeItem('token');
    window.location.reload();
  }
  const menu = (
    <Menu>
      <Menu.Item>
          <Icon type="user" /> Profile
      </Menu.Item>
      <Menu.Item onClick={logout}>
          <Icon type="poweroff" />Logout
      </Menu.Item>
    </Menu>
  );

  

  return (
    <div id="header">
      <div className="first-row">
        <div className="wrapper">
          <Row gutter={16}>
            <Col span={3}>
              <div className="logo-holder">
                <div className="logo-padder">
                  <h2>CONNECTSX</h2>
                </div>
              </div>
            </Col>
            <Col span={16}>
              {/* <div id="search-form">
                <Input
                  placeholder="Search Here"
                  prefix={<Icon type="search" />}
                />
              </div> */}
            </Col>
            <Col span={5}>
              <div className="user-settings">
                {/* <Icon type="notification" className="send-notification-icon" />
                <Popover trigger="click" overlayClassName="notifications-popover" content={<Notifications />}>
                  <Badge dot>
                    <Icon type="bell" className="notifications-icon" />
                  </Badge>
                </Popover> */}
                <Popover trigger="click" overlayClassName="user-profile-popover" content={menu}>
                  <span>
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    <span>{userReducer.first_name} {userReducer.last_name}</span>
                  </span>
                </Popover>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className="second-row">
        <div className="wrapper">
          <div className="main-menu">
            <Link to="/hospitals" className={active === 'Hospitals' ? 'active' : ''}>
              <Icon type="medicine-box" /> Hopitals
            </Link>
            <Link to="/conversations" className={active === 'Conversations' ? 'active' : ''}>
              <Icon type="message" /> Conversations
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = ({ userReducer }) => ({ userReducer });
export default connect(mapStateToProps, { getCurrentUserAction })(Header);

