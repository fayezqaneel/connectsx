import React from 'react';

const Privacy = (props) => {

  return (
    <div className="page-holder">
      <div id="container">
        <nav className="navbar">
          <a href="/">Home</a>
          <a className="active">Privacy</a>
          <a href="/contact-us">Contact us</a>
        </nav>
        <div id="background" />
        
        <div className="section">
        <a href="/"><div className="logo-holder"></div></a>
        <h1 id="title">Privacy Policy</h1>
        <div>

This privacy policy discloses the privacy practices for SurgeryLink mobile app and web site by CatchBeat Technologies LLC. This privacy policy applies solely to information collected by SurgeryLink Mobile Application and www.sugerylinkapp.com web site. It will notify you of the following:
<ol>
<li>What personally identifiable information is collected from you through the web site, how it is used and with whom it may be shared.</li>
<li>What choices are available to you regarding the use of your data.</li>
<li>The security procedures in place to protect the misuse of your information.</li>
<li>How you can correct any inaccuracies in the information.</li>
<li>How we safeguard and protect your information.</li>
</ol>
<h4>Information Collection, Use, and Sharing</h4>
We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you.

We will use your information to respond to you, regarding the reason you contacted us. Your information will also be available to business or venues within the system which you visit or request services from. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request. We may use your website or mobile app usage information in annonimized or at an aggregate level to provide your with new promotions and services.

Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.

<h4>Your Access to and Control Over Information</h4>
You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address info@surgerylinkapp.com
<ul>
   <li>See what data we have about you, if any.</li>

   <li>Change/correct any data we have about you.</li>

   <li>Have us delete any data we have about you.</li>

   <li>Express any concern you have about our use of your data.</li>
</ul>
<h4>Security</h4>
We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.

Wherever we collect sensitive information (such as your name, your phone number, email address), that information is encrypted and transmitted to us in a secure way over HTTPS. You can verify this by looking for a closed lock icon at the bottom of your web browser, or looking for "https" at the beginning of the address of the web page.

While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.

<h4>Registration</h4>

In order to use this website, a user must first complete the registration form. During registration a user is required to give certain information (such as name, email address, phone number). This information is used to contact you about the products/services on our site in which you have expressed interest.

<h4>Cookies</h4>

We use "cookies" on the website surgerylinkapp.com. A cookie is a piece of data stored on a site visitor's hard drive to help us improve your access to our site and identify repeat visitors to our site. For instance, when we use a cookie to identify you, you would not have to log in a password more than once, thereby saving time while on our site. Cookies can also enable us to track and target the interests of our users to enhance the experience on our site. Usage of a cookie is in no way linked to any personally identifiable information on our site. Similarly, on mobile device we use local storage to store a unique token to identify repeat visitors.

<h4>Updates</h4>

Our Privacy Policy may change from time to time and all updates will be posted on this page.

If you feel that we are not abiding by this privacy policy, you should contact us immediately via email at info@surgerylinkapp.com.
          </div>
      </div>
      </div>
    </div>
  );

};

export default Privacy;