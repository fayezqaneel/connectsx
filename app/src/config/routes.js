import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Login from '../components/login';
import Hospitals from '../components/hospitals';
import Conversations from '../components/conversations';
import Home from '../components/home';
import Privacy from '../components/privacy';
import Support from '../components/support';
// list of working routes for frontend app
export const routes = [
  {
    path: '/login',
    exact: true,
    component: Login,
    requiredAuth: false,
    requiredHeader: false,
  },
  {
    path: '/hospitals',
    exact: true,
    component: Hospitals,
    requiredAuth: true,
    requiredHeader: true,
  },
  {
    path: '/conversations',
    exact: true,
    component: Conversations,
    requiredAuth: true,
    requiredHeader: true,
  },
  {
    path: '/',
    exact: true,
    component: Home,
    requiredAuth: false,
    requiredHeader: false,
  },
  {
    path: '/privacy',
    exact: true,
    component: Privacy,
    requiredAuth: false,
    requiredHeader: false,
  },
  {
    path: '/contact-us',
    exact: true,
    component: Support,
    requiredAuth: false,
    requiredHeader: false,
  },
];

export const Auth = {
  isAuthenticated: window.localStorage.getItem('token') ? true : false,
  authenticate() {
    this.isAuthenticated = true;
  },
};

export const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        Auth.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}