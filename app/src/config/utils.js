import Store from "./store";
import { API_URL } from './constants';
const querystring = require('querystring');

// util function to sort array of objects based on attribute value
export function isLoggedIn() {
  const state = Store.getState();
  return window.localStorage.getItem('token') || (state && state.userReducer && state.userReducer.access);
}

export const addQuery = (props, query, pathname, replaceAll) => {
  pathname = pathname || props.location.pathname;
  const queryObj = querystring.parse(props.location.search.replace(/\?/g, ''));
  let searchParams = replaceAll ? query : {...queryObj, ...query};
  // returns the existing query string: '?type=fiction&author=fahid'
  Object.keys(searchParams).forEach((key) => {
      if (searchParams[key] === undefined || searchParams[key] === ''){
        delete searchParams[key];
      }
  });
  props.history.push({
    pathname: pathname,
    search: querystring.stringify(searchParams)
  });
 };

 export const getQueryParams = (props) => {
  return querystring.parse(props.location.search.replace(/\?/g, ''));
 };

 export const getQueryParam = (key, defaultValue) => {
  var urlParams = new URLSearchParams(window.location.search);
  return urlParams.has(key) ? urlParams.get(key) : defaultValue;
 };

 export const getApiLink = (id, objectName) => {
  return `/api/v1/${objectName}/${id}/`;
 };

 export const getUserToken = () => {
  return window.localStorage.getItem('token');
 };

