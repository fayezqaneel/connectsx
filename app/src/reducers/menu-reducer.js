
export default function reducer(state = { menus: [], meta: {} }, action) {
  switch (action.type) {
    // getting campaigns from server and load it into redux state
    case 'FETCH_MENUS': {
      return Object.assign({}, state, {
        meta: action.payload.meta,
        menus: action.payload.objects,
      });
    }
    default: {
      break;
    }
  }
  return state;
}
