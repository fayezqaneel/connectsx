
export default function reducer(state = { hospitals: [], meta: {}, selected_hospital: false }, action) {
  switch (action.type) {
    // getting campaigns from server and load it into redux state
    case 'FETCH_HOSPITALS': {
      return Object.assign({}, state, {
        meta: action.payload.meta,
        hospitals: action.payload.objects,
      });
    }
    case 'SELECT_HOSPITAL': {
      return Object.assign({}, state, {
        selected_hospital: action.payload
      });
    }
    case 'SELECT_HOSPITAL_BY_ID': {
      return Object.assign({}, state, {
        selected_hospital: state.hospitals.filter(hospital => hospital.id === action.payload)[0],
      });
    }
    case 'SAVE_HOSPITAL': {
      if (state.selected_hospital && state.selected_hospital.id || state.selected_hospital.randId) {
        return {
          ...state,
          hospitals: state.hospitals.map(
            hospital => hospital.id === action.payload.id || (hospital.randId && hospital.randId ===  action.payload.randId)
              ? action.payload
              : hospital
          ),
          selected_hospital: Object.assign({}, state.selected_hospital, action.payload),
        }
      } else {
        return {
          ...state,
          hospitals: state.hospitals.concat(action.payload),
          selected_hospital: Object.assign({}, state.selected_hospital, action.payload),
        }
      }
    }
    case 'DELETE_HOSPITAL': {
      return {
        ...state,
        hospitals: state.hospitals.filter(hospital => hospital.id !== action.payload &&  hospital.randId !== action.payload),
      };
    }
    default: {
      break;
    }
  }
  return state;
}