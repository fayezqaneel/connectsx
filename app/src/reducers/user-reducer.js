
export default function reducer(state = { access: false, refresh: false }, action) {
  switch (action.type) {
    // getting advertisers fro server and load it into redux state
    case 'LOGIN_USER': {
      return Object.assign({}, state, action.payload);
    }
    case 'GET_CURRENT_USER': {
      return Object.assign({}, state, action.payload);
    }
    default: {
      break;
    }
  }
  return state;
}
