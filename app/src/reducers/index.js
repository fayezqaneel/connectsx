import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import userReducer from './user-reducer';
import menuReducer from './menu-reducer';
import HospitalReducer from './hospital-reducer';
import ConversationReducer from './conversation-reducer';

export default combineReducers({
  routerReducer,
  userReducer,
  menuReducer,
  HospitalReducer,
  ConversationReducer
});
