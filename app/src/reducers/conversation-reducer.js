import { getQueryParam } from '../config/utils';
export default function reducer(state = { conversations: [], meta: {}, selected_conversation: false }, action) {
  switch (action.type) {
    // getting campaigns from server and load it into redux state
    case 'FETCH_CONVERSATIONS': {
      const page = Math.floor(action.payload.meta.offset / action.payload.meta.limit) + 1;
      const order_by = getQueryParam('order_by', '-id');
      return Object.assign({}, state, {
        meta: { ...state.meta, ...action.payload.meta, page, order_by },
        conversations: action.payload.objects,
      });
    }
    case 'SET_META': {
      return Object.assign({}, state, {
        meta: {...state.meta, ...action.payload},
      });
    }
    default: {
      break;
    }
  }
  return state;
}