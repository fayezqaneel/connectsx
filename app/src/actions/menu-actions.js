import axios from 'axios';
import { API_URL } from '../config/constants';
axios.defaults.headers.common['Authorization'] = `Bearer ${window.localStorage.getItem('token')}`;
/*
  action fuction to get all stats at once from server and dispatch it to redux
  reducer
*/
export function fetchMenusAction(cb) {
  return dispatch => {
    let url = `${API_URL}v1/menu/`;
    axios.get(url)
      .then((response) => {
        dispatch({ type: 'FETCH_MENUS', payload: response.data });
        // if callback function exists then pass the response to it and call it.
        cb && cb(response);
      })
      .catch((error) => {
        cb && cb(error.response.data.detail);
      });
  };
}

/*
  action fuction to get all stats at once from server and dispatch it to redux
  reducer
*/
export function createMenuItemAction(payload, cb) {
  return dispatch => {
    let url = `${API_URL}v1/menuitem/`;
    axios.post(url, payload)
      .then((response) => {
        // if callback function exists then pass the response to it and call it.
        cb && cb(response);
      })
      .catch((error) => {
        cb && cb(error.response.data.detail);
      });
  };
}