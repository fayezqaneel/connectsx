import axios from 'axios';
import { API_URL } from '../config/constants';
import { Auth } from '../config/routes';
import { useAction } from './base-actions';

/*
  action fuction to get advertisers list from server and dispatch it to redux
  reducer
*/
export function loginUserAction(values, cb, err) {
  return dispatch => {
    let url = `${API_URL}token/`;
    axios.post(url, values)
      .then((response) => {
        // dispatch data to reducer
        dispatch({ type: 'LOGIN_USER', payload: response.data });
        window.localStorage.setItem('token', response.data.access);
        axios.defaults.headers.common['Authorization'] = `Bearer ${window.localStorage.getItem('token')}`;
        Auth.authenticate();
        // if callback function exists then pass the response to it and call it.
        cb && cb(response);
      })
      .catch((error) => {
        err && err(error);
      });
  };
}
export const getCurrentUserAction = (cb) => useAction({
  url: 'me/',
  method: 'get',
  action: 'GET_CURRENT_USER'
});