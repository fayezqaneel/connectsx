import { useAction } from './base-actions';

export const fetchHospitalsAction = cb => useAction({
  url: 'hospital/',
  method: 'get',
  action: 'FETCH_HOSPITALS',
  cb
});

export const selectHospitalAction = hospital => useAction({
  normalAction: true,
  action: 'SELECT_HOSPITAL',
  payload: hospital
});

export const selectHospitalByIdAction = id => useAction({
  normalAction: true,
  action: 'SELECT_HOSPITAL_BY_ID',
  payload: parseInt(id) 
});

export const saveHospitalAction = (hospital, cb) => useAction({
  url: `hospital/${hospital.id}/`,
  method: 'put',
  action: 'SAVE_HOSPITAL',
  reqPayload: hospital,
  cb
});

export const addHospitalAction = (hospital, cb) => useAction({
  url: 'hospital/',
  method: 'post',
  action: 'SAVE_HOSPITAL',
  reqPayload: hospital,
  cb
});
export const deleteHospitalAction = (hospital, cb) => useAction({
  url: `hospital/${hospital.id}/`,
  method: 'delete',
  action: 'DELETE_HOSPITAL',
  payload: hospital.id,
  cb
});