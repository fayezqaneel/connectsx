
import axios from 'axios';
import { API_URL } from '../config/constants';
axios.defaults.headers.common['Authorization'] = `Bearer ${window.localStorage.getItem('token')}`;


export function fetchConversationsAction(search, cb) {
  return dispatch => {
    let url = `${API_URL}v1/conversation/`;
    url = search ? `${url}${search}` : url;
    axios.get(url)
      .then((response) => {
        dispatch({ type: 'FETCH_CONVERSATIONS', payload: response.data });
        // if callback function exists then pass the response to it and call it.
        cb && cb(response);
      })
      .catch((error) => {
        if (error.response && error.response.status === 400) {
          // if callback function exists then pass the error to it and call it.
          cb && cb(error);
        }
      });
  };
}

export function setMetaAction (payload, cb) {
  return dispatch => {
    dispatch({ type: 'SET_META', payload });
    cb && cb();
  };
}